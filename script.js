'use strict';

const searchButton = document.getElementById("search-country-btn");
searchButton.addEventListener("click", function() {
    const counrtyForm = document.getElementById("search-form");
    let data = new FormData(counrtyForm);
    if(data.get("country_name")==""||data.get("country_name").length<2){
        alert("Form is empty or too short (min 2 symbols)")
        clearForm();
    }else{
        findCountry(data);
    }
});

const baseUrl = 'https://restcountries.eu/rest/v2/name/';


async function findCountry(data) {
    let country_name = data.get("country_name").toString();
    console.log(country_name);
    const response = await fetch(baseUrl + country_name);
    if(response.ok) {
        let country_info = await response.json();
        let nc = new Country(country_info[0].name, country_info[0].capital, country_info[0].flag, country_info[0].region, country_info[0].currencies[0].name);
        let ncElem = createCountryElement(nc);
        document.getElementById("country-container").prepend(ncElem);
        clearForm();
    } else {
        alert("Sorry, Country with this name doesn't exist");
        clearForm();
    }
}

function clearForm(){
    document.getElementsByName("country_name")[0].value ="";
    document.getElementsByName("country_name")[0].focus();
}

class Country{
    constructor(name, capital, flag, region, currency) {
        this.name = name;
        this.capital = capital;
        this.flag = flag;
        this.region = region;
        this.currency = currency;
    }
}

function createCountryElement(country){
    let content = `
                    <div>
                        <p class = "countries"> Counrty: </p> <h2 class = "countries">` + country.name + `</h2>
                    </div>
                    <div>
                        <p  class = "countries"> Capital: </p>
                        <p class = "countries">` + country.capital + `</p>
                    </div>
                    <div>
                        <p  class = "countries"> Flag: </p>
                        <img class=flag-image" src=`+country.flag+` width="300" height="180" alt="Post image">
                    </div>
                    <div>
                        <p  class = "countries"> Region: </p>
                        <p class = "countries">` + country.region + `</p>
                    </div>
                    <div>
                        <p  class = "countries"> Currency: </p>
                        <p class = "countries">` + country.currency + `</p>
                    </div>
                    <div>
                        <p  class = "countries"> Find more: </p>
                        <a href="https://www.google.com/search?q=`+country.name+`" target="_blank">More</a>
                    </div>
                    `;
    let element = document.createElement('div');
    element.innerHTML = content;
    element.classList.add("card");
    return element;
}
